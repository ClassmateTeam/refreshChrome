# refreshChrome

Removes a user's `~/Library/Application Support/Google/Chrome/` directory at login, resulting in a fresh install of Google Chrome.
